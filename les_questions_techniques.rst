Les questions technique pour Clément
====================================

- Initialement, quel était l'usage des "modals" de django fm situé en bas de la page d'accueil ?
- Quels type peut avoir un `Lieu` ? À quoi cela sert-il dans l'application ? Est-ce que c'est pertinent d'avoir un modèle séparé ?
- Y avait-il un formulaire de connexion dans le menu ? Qu'est-ce qu'il se situait dans le menu, notamment en haut à droite du menu ?
- Les activity_stream, cela vous rendez quoi exactement ?
